


#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <cstring>
#include <QtCore/qmath.h>
#include "liblinear/linear.h"

#include "pedestrian_learning.h"
#define PI 3.14159265
#define Malloc(type,n) (type *)malloc((n)*sizeof(type))
#define NUM_FEATURES 2250
#define MAX_IMAGE_NUMBER 600

using namespace std;


void pedestrian_learning::setVsize(QVector < QVector <int> > &matrix){//side private function
    matrix.resize(image.height());
    for (int i=0; i<image.height(); i++)
        matrix[i].resize(image.width());
}

void pedestrian_learning::setdoubleVsize(QVector < QVector <double> > &matrix){//side private function
    matrix.resize(image.height());
    for (int i=0; i<image.height(); i++)
        matrix[i].resize(image.width());
}

int runFilter(QVector< QVector<int> > &imgC,int imgWidth, QVector< QVector<double> > &hogS, QVector< QVector<double> >& hogV){
        double tmp_x,tmp_y;
        for (int y=1; y<199; y++)
                for (int x=1; x<imgWidth-1; x++){
                        tmp_x=imgC[y][x+1]-imgC[y][x-1];
                        tmp_y=imgC[y+1][x]-imgC[y-1][x];
                        hogS[y][x]=sqrt(tmp_x*tmp_x+tmp_y*tmp_y);
                        hogV[y][x]=abs(atan2(tmp_y,tmp_x) * 179 / PI);
                }
                return 1;
}


int rgbSV2Hist(QVector< QVector<double> > &hogSR,QVector< QVector<double> > &hogVR,QVector< QVector<double> > &hogSG, QVector< QVector<double> >  &hogVG,QVector< QVector<double> > &hogSB, QVector< QVector<double> > &hogVB,int imgWidth, QVector< QVector<int> > &imgHOG){
        double tmpV,tmpS;
        for (int y=1; y<199; y++)
                for (int x=1; x<imgWidth-1; x++){
                        tmpS=hogSR[y][x];
                        tmpV=hogVR[y][x];
                        if (hogSG[y][x]>tmpS){
                                tmpS=hogSG[y][x];
                                tmpV=hogVG[y][x];
                        }
                        if (hogSB[y][x]>tmpS){
                                tmpS=hogSB[y][x];
                                tmpV=hogVB[y][x];
                        }
                        imgHOG[y][x]=(int) (tmpV / 20);
                }
                return 0;
}

int countImgFeach(QVector< QVector<int> > &imgHOG,int xLeft,QVector<int> &trainfeatures){
        int lastFNum=0;
        int hist8x8[9]={0,0,0,0,0,0,0,0,0};
        for (int ybase=1; ybase<200; ybase+=8)
                for (int xbase=xLeft; xbase<xLeft+80; xbase+=8){
                    for (int i=0;i<9;i++) hist8x8[i]=0;
                    for (int yshift=0; yshift<8; yshift++)
                        for (int xshift=0; xshift<8; xshift++)
                                  ++hist8x8[ imgHOG[ybase+yshift][xbase+xshift]   ];
                    for (int i=0; i<9;i++){
                              trainfeatures[lastFNum]=hist8x8[i];
                             ++lastFNum;
                    }
        }

        return lastFNum;
}






int pedestrian_learning::learning(int largc, char *largv[]){
    if (largc<3 || largc>4){
                QMessageBox::information(this, "learning","wrong number of args");
                exit(1);
    }
        //acrgv: prog.exe inputdirectory locationfile [bootstrapping]
        //char  arg0[] = "programName";
        //char  arg1[] = "learn";
        //char  arg2[] = "C:\\temp\\images";
        //char  arg3[] = "C:\\temp\\locatefile.idl";
        //*  largv[] = { &arg0[0], &arg1[0], &arg2[0],&arg3[0], NULL };
        //int   largc   = (int)(sizeof(largv) / sizeof(largv[0])) - 1;

    int arg_dir=1, arg_location_file=2;
      //  cout<<"\n state:learning\nimage dir:"<<largv[arg_dir]<<"\n pedestrian location file:" <<largv[arg_location_file];


        ////READ locationFILE
        int locMap[MAX_IMAGE_NUMBER];
        for (int i=0; i<200; i++) locMap[i]=0;
        int num_of_inputs=0;
        int numImage=0,y2=0,x2=0,y1=0,x1=0;


        ifstream locFile;
        locFile.open(largv[arg_location_file],ifstream::in);
        if (locFile.bad()){
                    QMessageBox::information(this, "learning","can't open pedestrian location file");
                    exit(1);
        }
        while (locFile.good()){
                        numImage=0;y2=0;x2=0;y1=0;x1=0;
                        locFile>>numImage>>y1>>x1>>y2>>x2;
                        locMap[numImage]=x1;
                        num_of_inputs++;
                }
        locFile.close();

//        //// FILL TRAIN STRUCTURE AND TRAIN
        int lastTrainImgNum=0;
        struct problem myprob;
                myprob.l = int(num_of_inputs*4); // number of instances
               myprob.bias = -1; // bias feature
                myprob.n = NUM_FEATURES; // number of features + one for bias if needed
                myprob.y = Malloc(int, myprob.l); // allocate space for labels
                myprob.x = Malloc(struct feature_node *, myprob.l); // allocate space for features

       struct parameter param;
          param.solver_type = L2R_L2LOSS_SVC_DUAL;
          param.C = 1;
          param.eps = 1e-4;
          param.nr_weight = 0;
          param.weight_label = NULL;
          param.weight = NULL;

        ////Start reading images from IMAGE directiry
        ifstream indir;//������� ���� �� ������� ��������
        char tmp[255],shortImgName[255];
        strcpy(tmp,largv[arg_dir]);
        strcat(tmp,"\\files.dir");
        indir.open(tmp,ifstream::in);
        if (indir.bad()){
                    QMessageBox::information(this, "learning","can't open diles.dir");
                    exit(1);
        }



        while (indir.good()){
        indir>>shortImgName;//���� ���� ����� �������� �������� �������� � ���� ����������
        int shortImgNum=atoi(shortImgName);
                strcpy(tmp,largv[arg_dir]);
                strcat(tmp,"\\");
                strcat(tmp,shortImgName);
                strcat(tmp,".png");
                 image.load(tmp);
                 if (image.isNull()) {
                          QMessageBox::information(this, "Learning","bad directory. Cannot load image file from diles.dir");
                          exit (1);
                 }
                 setVsize(imgR);setVsize(imgG); setVsize(imgB);



               //���� ������� ����������� �� 3 �������

                QRgb pointColor;
                    for (int y=0; y<image.height();y++)
                        for (int x=0; x<image.width(); x++ ){
                            pointColor=image.pixel(x,y);
                            imgR[y][x]=qRed(pointColor);
                            imgG[y][x]=qGreen(pointColor);
                            imgB[y][x]=qBlue(pointColor);
                        }

                        //������ ������� ����))
                        setdoubleVsize(hogRS); setdoubleVsize(hogRV);
                        setdoubleVsize(hogGS); setdoubleVsize(hogGV);
                        setdoubleVsize(hogBS); setdoubleVsize(hogBV);

                        runFilter(imgR,image.width(),hogRS,hogRV);
                        runFilter(imgG,image.width(),hogGS,hogGV);
                        runFilter(imgB,image.width(),hogBS,hogBV);

                        setVsize(InImageHOG);
                        rgbSV2Hist(hogRS,hogRV,hogGS,hogGV,hogBS,hogBV,image.width(),InImageHOG);
                        //��������� ����

                        //������� �������� � ��������� � �������� �������


                        int ret = locMap[atoi(shortImgName)];
                       //����������� �� ���� ����� �� �������� shortImgName
                        myprob.x[lastTrainImgNum] = Malloc(struct feature_node, NUM_FEATURES+1);
                        myprob.x[lastTrainImgNum][NUM_FEATURES].index = -1;  // -1 marks the end of list

                        tmpTrainFeatures.resize(NUM_FEATURES+5);
                        countImgFeach(InImageHOG,ret,tmpTrainFeatures);
                        for (int j = 0; j < NUM_FEATURES; j++) {
                            myprob.x[lastTrainImgNum][j].index = 1+j; // 1-based feature number
                            myprob.x[lastTrainImgNum][j].value = (double) tmpTrainFeatures[j];
                        }
                        myprob.y[lastTrainImgNum] = 1;
                        lastTrainImgNum++;

                        //���� �������� ��� ��������� �������� ��������
                      for (int two=0; two<2; two++){
                        int background1=10;
                        do{
                            background1 = qRound((qrand()*1.0/RAND_MAX)*image.width());
                            if (background1>image.width()-90) background1-=90;
                        } while (abs(background1 - ret)<50);

                        myprob.x[lastTrainImgNum] = Malloc(struct feature_node, NUM_FEATURES+1);
                        myprob.x[lastTrainImgNum][NUM_FEATURES].index = -1;  // -1 marks the end of list

                        tmpTrainFeatures.resize(NUM_FEATURES+5);
                        countImgFeach(InImageHOG,background1,tmpTrainFeatures);
                        for (int j = 0; j < NUM_FEATURES; j++) {
                            myprob.x[lastTrainImgNum][j].index = 1+j; // 1-based feature number
                            myprob.x[lastTrainImgNum][j].value = (double) tmpTrainFeatures[j];
                        }
                        myprob.y[lastTrainImgNum] = -1;
                        lastTrainImgNum++;
                        //�������� ��������� ���
                      }
        }

      myprob.l=lastTrainImgNum;

      struct model* pedestrClassify = train(&myprob, &param);

  // SAVE MODEL FOR PREDICTION
    save_model("pedest_model.svm", pedestrClassify);

    free_and_destroy_model(&pedestrClassify);
    destroy_param(&param);
    free(myprob.y);
    for (int i = 0; i < myprob.l; i++) free(myprob.x[i]);
    free(myprob.x);

    QMessageBox::information(this, "Learning","DONE! model in 'pedest_model.svm' ");
    return 0;
}


pedestrian_learning::pedestrian_learning(QWidget *parent)
    : QMainWindow(parent)
{

        imageLabel = new QLabel;
        imageLabel->setBackgroundRole(QPalette::Base);

        imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
        imageLabel->setScaledContents(false);

        setCentralWidget(imageLabel);
        setWindowTitle("People Recognition");
}

pedestrian_learning::~pedestrian_learning()
{

}
