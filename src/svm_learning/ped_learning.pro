#-------------------------------------------------
#
# Project created by QtCreator 2011-10-17T21:37:19
#
#-------------------------------------------------

QT       += core gui

TARGET = ped_learning
TEMPLATE = app


SOURCES += main.cpp\
        pedestrian_learning.cpp \
    liblinear/tron.cpp \
    liblinear/linear.cpp \
    liblinear/blas/dscal.c \
    liblinear/blas/dnrm2.c \
    liblinear/blas/ddot.c \
    liblinear/blas/daxpy.c

HEADERS  += pedestrian_learning.h \
    liblinear/tron.h \
    liblinear/linear.h \
    liblinear/blas/blasp.h \
    liblinear/blas/blas.h




