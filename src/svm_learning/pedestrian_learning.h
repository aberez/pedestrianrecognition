#ifndef PEDESTRIAN_LEARNING_H
#define PEDESTRIAN_LEARNING_H

#include <QtGui>

class pedestrian_learning : public QMainWindow
{
    Q_OBJECT

public:
    pedestrian_learning(QWidget *parent = 0);
    ~pedestrian_learning();

      int learning(int,char**);
private:
      QLabel *imageLabel;
      QImage image;

       void setVsize(QVector < QVector <int> > &matrix);
       void    setdoubleVsize(QVector < QVector <double> > &matrix);
       QVector< QVector<int> > imgR,imgG,imgB;
       QVector< QVector<double> > hogRS,hogRV,hogGS,hogGV;
       QVector< QVector<double> > hogBS,hogBV;
       QVector< QVector<int> > InImageHOG;
       QVector <int> tmpTrainFeatures;
};

#endif // PEDESTRIAN_LEARNING_H
