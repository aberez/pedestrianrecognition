#include <QtCore/QCoreApplication>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <Qstring>
#include <map>
#include <cmath>
#include <QVector>

using namespace std;

int main(int argc, char *argv[])
{//progname myfile rightfile
       // int c; cin>>c;
       //  char  arg0[] = "programName";
       //  char  arg1[] = "C:\\temp\\marking.dir";
       //  char  arg2[] = "C:\\temp\\learn-images\\train-processed.idl";
       //  char*  largv[] = { &arg0[0], &arg1[0], &arg2[0], NULL };
       //  int   largc   = (int)(sizeof(largv) / sizeof(largv[0])) - 1;
    //progname myfile rightfile
    QCoreApplication a(argc, argv);
    if (argc!=3){ cout<<"wrong number of args. need format: myfile rightfile\n";
        return 1;
    }

    ifstream myFile,rightFile;
     myFile.open(argv[1],ifstream::in);
     //myFile.open(largv[1],ifstream::in);
    if (!myFile){
        cout<<"can't open myFile";
        return 1;
     }
    rightFile.open(argv[2],ifstream::in);
  //  rightFile.open(largv[2],ifstream::in);
    if (!rightFile){
        cout<<"can't open myFile";
        return 1;
     }

      multimap<int,int> myMap, rightMap;
      multimap<int,int>::iterator myIt,rightIt;
      QVector <multimap<int,int>::iterator> toDel;
      pair<multimap<int,int>::iterator,multimap<int,int>::iterator> myRet, rightRet;

      int mNum,mXl,mXr,mYu,mYd;
      int rNum,rXl,rXr,rYu,rYd;
      int GT=0,DET=0;
      int TP=0, FP=0, FN=0;
      while (myFile.good()){
          myFile>>mNum>>mYu>>mXl>>mYd>>mXr;
          myMap.insert(make_pair(mNum,mXl));
          //DET++;//����� �����������
      }
//      while (rightFile.good()){
//          rightFile>>rNum>>rYu>>rXl>>rYd>>rXr;
//          rightMap.insert(make_pair(rNum,rXl));
//          GT++; //������ ����� ���������
//      }

       while (rightFile.good()){
               rightFile>>rNum>>rYu>>rXl>>rYd>>rXr;
               GT++;
               myRet = myMap.equal_range(rNum);
               int oneTP=0;
               toDel.clear();
               for (myIt=myRet.first; myIt!=myRet.second; ++myIt){//������� ��� ��������� 1 ��������
                   if (abs((double)rXl-(double)(*myIt).second)<=40){
                       oneTP=1;
                       toDel.push_back(myIt);
                   }
               }
               for (int k=0; k<toDel.size(); k++) myMap.erase(toDel[k]);
               TP+=oneTP;
               FN=FN+(1-oneTP);//�� ��������� ���������, � �� ����
      }
      FP=myMap.size();
      double prec=TP*1.0/(TP+FP);
      double recall=TP*1.0/GT;
      double F=2*prec*recall/(prec+recall);
      cout<<"precision is "<<prec<<endl;
      cout<<"recall is "<<recall<<endl;
      cout<<"F="<<F<<endl;





    myFile.close();
    rightFile.close();
    return a.exec();
}
