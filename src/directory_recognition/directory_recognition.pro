#-------------------------------------------------
#
# Project created by QtCreator 2011-10-17T17:48:47
#
#-------------------------------------------------

QT       += core gui

TARGET = directory_recognition
TEMPLATE = app


SOURCES += main.cpp\
        dir_classify.cpp \
    liblinear/tron.cpp \
    liblinear/linear.cpp \
    liblinear/blas/dscal.c \
    liblinear/blas/dnrm2.c \
    liblinear/blas/ddot.c \
    liblinear/blas/daxpy.c

HEADERS  += dir_classify.h \
    liblinear/tron.h \
    liblinear/linear.h \
    liblinear/blas/blasp.h \
    liblinear/blas/blas.h




