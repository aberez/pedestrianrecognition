#ifndef DIR_CLASSIFY_H
#define DIR_CLASSIFY_H

#include <QtGui/QMainWindow>
#include <QtGui>

class dir_classify : public QMainWindow
{
    Q_OBJECT

public:
    dir_classify(QWidget *parent = 0);
    ~dir_classify();
    //int process();
    int process(int largc,char** largv);

    void setVsize(QVector < QVector <int> > &matrix);
     void    setdoubleVsize(QVector < QVector <double> > &matrix);
     int countCellFeature(QVector< QVector <int> >&ImageHOG,QVector< QVector< QVector<int> > >&ImgCellHOG);

     QVector< QVector<int> > imgR,imgG,imgB;
     QVector< QVector<double> > hogRS,hogRV,hogGS,hogGV;
     QVector< QVector<double> > hogBS,hogBV;
     QVector< QVector<int> > InImageHOG;
     QVector< QVector< QVector <int> > >ImgCellHOG;
     QVector <int> tmpTestFeatures;
     QVector <std::pair<int,double> >ImgPredValues, PedestrList;

private:
    QLabel *imageLabel;
    QImage image;
};

#endif // DIR_CLASSIFY_H
