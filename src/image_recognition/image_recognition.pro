#-------------------------------------------------
#
# Project created by QtCreator 2011-10-17T14:05:56
#
#-------------------------------------------------

QT       += core gui

TARGET = image_recognition
TEMPLATE = app


SOURCES += main.cpp\
        image_classify.cpp \
    liblinear/tron.cpp \
    liblinear/linear.cpp \
    liblinear/blas/dscal.c \
    liblinear/blas/dnrm2.c \
    liblinear/blas/ddot.c \
    liblinear/blas/daxpy.c

HEADERS  += image_classify.h \
    liblinear/tron.h \
    liblinear/linear.h \
    liblinear/blas/blasp.h \
    liblinear/blas/blas.h




