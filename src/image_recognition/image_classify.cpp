#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <cstring>
#include <QtCore/qmath.h>
#include "liblinear/linear.h"
#include "image_classify.h"


#define PI 3.14159265
#define Malloc(type,n) (type *)malloc((n)*sizeof(type))
#define NUM_FEATURES 2250
#define MAX_IMAGE_NUMBER 600
#define IMAGE_THRESHOLD 0.2

using namespace std;

void image_classify::setVsize(QVector < QVector <int> > &matrix){//side private function
    matrix.resize(image.height());
    for (int i=0; i<image.height(); i++)
        matrix[i].resize(image.width());
}

void image_classify::setdoubleVsize(QVector < QVector <double> > &matrix){//side private function
    matrix.resize(image.height());
    for (int i=0; i<image.height(); i++)
        matrix[i].resize(image.width());
}

int runFilter(QVector< QVector<int> > &imgC,int imgWidth, QVector< QVector<double> > &hogS, QVector< QVector<double> >& hogV){
        double tmp_x,tmp_y;
        for (int y=1; y<199; y++)
                for (int x=1; x<imgWidth-1; x++){
                        tmp_x=imgC[y][x+1]-imgC[y][x-1];
                        tmp_y=imgC[y+1][x]-imgC[y-1][x];
                        hogS[y][x]=sqrt(tmp_x*tmp_x+tmp_y*tmp_y);
                        hogV[y][x]=abs(atan2(tmp_y,tmp_x) * 179 / PI);
                }
                return 1;
}


int rgbSV2Hist(QVector< QVector<double> > &hogSR,QVector< QVector<double> > &hogVR,QVector< QVector<double> > &hogSG, QVector< QVector<double> >  &hogVG,QVector< QVector<double> > &hogSB, QVector< QVector<double> > &hogVB,int imgWidth, QVector< QVector<int> > &imgHOG){
        double tmpV,tmpS;
        for (int y=1; y<199; y++)
                for (int x=1; x<imgWidth-1; x++){
                        tmpS=hogSR[y][x];
                        tmpV=hogVR[y][x];
                        if (hogSG[y][x]>tmpS){
                                tmpS=hogSG[y][x];
                                tmpV=hogVG[y][x];
                        }
                        if (hogSB[y][x]>tmpS){
                                tmpS=hogSB[y][x];
                                tmpV=hogVB[y][x];
                        }
                        imgHOG[y][x]=(int) (tmpV / 20);
                }
                return 0;
}

int image_classify::countCellFeature(QVector< QVector <int> >&ImageHOG,QVector< QVector< QVector<int> > >&ImgCellHOG){
    ImgCellHOG.clear();
    ImgCellHOG.resize(200/8);
    for (int ybase=1; ybase<200; ybase+=8){
       QVector< QVector <int> >histbyx;
       histbyx.resize(image.width()/8);
       for (int xbase=1; xbase<image.width()-10; xbase+=8){
           QVector<int>hist8x8(9,0);
           for (int yshift=0; yshift<8; yshift++)
               for (int xshift=0; xshift<8; xshift++)
                         ++hist8x8[ ImageHOG[ybase+yshift][xbase+xshift]   ];
           histbyx[xbase/8]=hist8x8;
       }
       ImgCellHOG[ybase/8]=histbyx;
    }
    return 0;
}


int countImgFeach(QVector< QVector< QVector<int> > >&ImgCellHOG,int xLeft,QVector<int> &trainfeatures){
        int lastFNum=0;
        for (int ybase=1; ybase<200; ybase+=8)
                for (int xbase=xLeft; xbase<xLeft+80; xbase+=8){
                   for (int i=0; i<9;i++){
                       trainfeatures[lastFNum]=ImgCellHOG[ybase/8][xbase/8][i];
                             ++lastFNum;
                    }
        }

        return lastFNum;
}

int cutNoMax(QVector <pair<int,double> >&InValues, QVector <pair<int,double> > &resVec){
    double maxval=0;
    resVec.clear();
    for (int i=0; i<InValues.size(); i++){
        if (InValues[i].second>IMAGE_THRESHOLD){
            if (InValues[i].second>maxval) maxval=InValues[i].second;
                else{
                resVec.push_back(InValues[i-1]);
                int j=i-2;
                while (j>=0 && InValues[j].second>IMAGE_THRESHOLD){
                  InValues[j].second=IMAGE_THRESHOLD-0.01;
                  j--;
                }
                j=i;
                while (j<InValues.size() && InValues[j].second>IMAGE_THRESHOLD){
                  InValues[j].second=IMAGE_THRESHOLD-0.01;
                  j++;
                }
            }

        }
    }
    return resVec.size();
}

image_classify::image_classify(QWidget *parent)
    : QMainWindow(parent)
{
    imageLabel = new QLabel;
    imageLabel->setBackgroundRole(QPalette::Base);
    setCentralWidget(imageLabel);
    setWindowTitle("Pedestrian Recognition");
}



//int image_classify::process(){
    int image_classify::process(int largc,char** largv){
//        char  arg0[] = "programName";
//        char  arg1[] = "C:\\temp\\pedest_model.svn";
//        char  arg2[] = "C:\\temp\\images\\150.png";
//        char* largv[] = { &arg0[0], &arg1[0], &arg2[0],NULL };
//        int   largc   = (int)(sizeof(largv) / sizeof(largv[0])) - 1;
        int   arg_model=1, arg_image=2;

        struct model* myModel;
        if((myModel=load_model(largv[arg_model]))==0)
          {
             QMessageBox::information(this, "image classify","can't load model from file" );
             exit(1);
          }


        image.load(largv[arg_image]);
        if (image.isNull()) {
                   QMessageBox::information(this, "image classify","Cannot load image file (see readme)");
                   exit (1);
               }
        setVsize(imgR);setVsize(imgG); setVsize(imgB);

        //���� ������� ����������� �� 3 �������

         QRgb pointColor;
         for (int y=0; y<image.height();y++){
             for (int x=0; x<image.width(); x++ ){
                 pointColor=image.pixel(x,y);
                 imgR[y][x]=qRed(pointColor);
                 imgG[y][x]=qGreen(pointColor);
                 imgB[y][x]=qBlue(pointColor);
             }
         }
         //������ ������� ����))

         setdoubleVsize(hogRS); setdoubleVsize(hogRV);
         setdoubleVsize(hogGS); setdoubleVsize(hogGV);
         setdoubleVsize(hogBS); setdoubleVsize(hogBV);

         runFilter(imgR,image.width(),hogRS,hogRV);
         runFilter(imgG,image.width(),hogGS,hogGV);
         runFilter(imgB,image.width(),hogBS,hogBV);

         setVsize(InImageHOG);
         rgbSV2Hist(hogRS,hogRV,hogGS,hogGV,hogBS,hogBV,image.width(),InImageHOG);
         //��������� ����

         //������� ��������
         int corr = 0;
         struct feature_node* x = Malloc(struct feature_node, NUM_FEATURES+1);
         x[NUM_FEATURES].index = -1;  // -1 marks the end of list
         double prob_estimates[1];

         //setVsize(InImageHOG);
         countCellFeature(InImageHOG,ImgCellHOG);
         ImgPredValues.clear();
         for (int xLeft=1; xLeft<image.width()-90; xLeft+=8){
             tmpTestFeatures.resize(NUM_FEATURES+5);
             countImgFeach(ImgCellHOG,xLeft,tmpTestFeatures);
             for (int j = 0; j < NUM_FEATURES; j++) {
                 x[j].index = 1+j; // 1-based feature number
                 x[j].value = (double) tmpTestFeatures[j];
             }

             predict_values(myModel,x,prob_estimates);
             ImgPredValues.push_back(make_pair(xLeft,prob_estimates[0]));

             //if (prob_estimates[0]>0.2)
             //   cout <<"xleft is "<<xLeft<<" .human. confidence: "  <<prob_estimates[0]<< endl; // confidence for the first class (+1, in our case)
             //    else  cout <<"xleft is "<<xLeft<<" .backgroung. confidence"  <<prob_estimates[0]<< endl;
         }
         cutNoMax(ImgPredValues,PedestrList);
         free(x);
         free_and_destroy_model(&myModel);


         //QRgb pointColor;//������ ������� �����
         for (int i=0; i<PedestrList.size(); i++){
                for (int y=0; y<image.height();y++){
                    pointColor=qRgb(0,255,0);
                    image.setPixel(PedestrList[i].first,y,pointColor);
                    image.setPixel(PedestrList[i].first+80,y,pointColor);
                }
         }
         imageLabel->resize(image.width(),image.height());
         imageLabel->setPixmap(QPixmap::fromImage(image));


}


image_classify::~image_classify()
{

}
