#ifndef IMAGE_CLASSIFY_H
#define IMAGE_CLASSIFY_H

#include <QtGui/QMainWindow>
#include <QtGui>

class image_classify : public QMainWindow
{
    Q_OBJECT

public:
    image_classify(QWidget *parent = 0);
   // int process();
    int process(int largc,char** largv);
    ~image_classify();

    void setVsize(QVector < QVector <int> > &matrix);
     void    setdoubleVsize(QVector < QVector <double> > &matrix);
     int countCellFeature(QVector< QVector <int> >&ImageHOG,QVector< QVector< QVector<int> > >&ImgCellHOG);

     QVector< QVector<int> > imgR,imgG,imgB;
     QVector< QVector<double> > hogRS,hogRV,hogGS,hogGV;
     QVector< QVector<double> > hogBS,hogBV;
     QVector< QVector<int> > InImageHOG;
     QVector< QVector< QVector <int> > >ImgCellHOG;
     QVector <int> tmpTestFeatures;
     QVector <std::pair<int,double> >ImgPredValues, PedestrList;

private:
    QLabel *imageLabel;
    QImage image;
};

#endif // IMAGE_CLASSIFY_H
