#Pedestrian Recognition#

The Pedestrian Recognition program finds pedestrians on photos. The detector is a machine learning algorithm (more specifically: SVM). Recognition model trains on tagged dataset. After that untagged images are processed by trained model.


For more information about this task go to [graphicon course page](http://courses.graphicon.ru/main/cg/2011/assignment02).